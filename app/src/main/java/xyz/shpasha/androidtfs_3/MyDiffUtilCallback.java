package xyz.shpasha.androidtfs_3;

        import android.support.v7.util.DiffUtil;

        import java.util.List;

public class MyDiffUtilCallback extends DiffUtil.Callback {

    private final List<Worker> oldList;
    private final List<Worker> newList;

    public MyDiffUtilCallback(List<Worker> oldList, List<Worker> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int i, int i1) {
        Worker w1 = oldList.get(i);
        Worker w2 = newList.get(i1);
        return w1.getName().equals(w2.getName());
    }

    @Override
    public boolean areContentsTheSame(int i, int i1) {
        Worker w1 = oldList.get(i);
        Worker w2 = newList.get(i1);
        return w1.getName().equals(w2.getName()) &&
                w1.getPosition().equals(w2.getPosition()) &&
                w1.getAge().equals(w2.getAge());
    }
}

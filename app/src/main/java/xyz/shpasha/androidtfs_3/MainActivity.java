package xyz.shpasha.androidtfs_3;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MyRecyclerAdapter adapter;
    private List<Worker> workers;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton sortAlphButton, sortIdButton, fab;
    private int lastId = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sortAlphButton = findViewById(R.id.sortAlphButton);
        sortIdButton = findViewById(R.id.sortIdButton);

        workers = new ArrayList<>();


        final RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new MyRecyclerAdapter(this, workers);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new ItemOffsetDecoration(5));

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Worker w = WorkerGenerator.generateWorker();
                w.setId(lastId++);
                workers.add(w);
                adapter.notifyItemInserted(adapter.getItemCount());
            }
        });

        sortAlphButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.dispatch(sortByALph());
            }
        });

        sortIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.dispatch(sortById());
            }
        });
    }

    List<Worker> sortById() {
        List<Worker> newList = new ArrayList<>(adapter.getData());
        Collections.sort(newList, new Comparator<Worker>() {
            @Override
            public int compare(Worker w1, Worker w2) {
                return w1.getId().compareTo(w2.getId());
            }
        });
        return newList;
    }

    List<Worker> sortByALph() {
        List<Worker> newList = new ArrayList<>(adapter.getData());
        Collections.sort(newList, new Comparator<Worker>() {
            @Override
            public int compare(Worker w1, Worker w2) {
                return w1.getName().compareTo(w2.getName());
            }
        });
        return  newList;
    }


}

package xyz.shpasha.androidtfs_3;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
    private int offset;

    public ItemOffsetDecoration(int offset) {
        this.offset = offset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.right = offset;
        outRect.left = offset;
        outRect.top = offset;
        outRect.bottom = offset;
    }
}

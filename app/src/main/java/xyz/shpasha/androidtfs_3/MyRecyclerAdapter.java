package xyz.shpasha.androidtfs_3;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private List<Worker> workers;
    private LayoutInflater inflater;

    public MyRecyclerAdapter(Context context, List<Worker> list) {
        this.inflater = LayoutInflater.from(context);
        this.workers = list;
    }

    @NonNull
    @Override
    public MyRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.card, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Worker w = workers.get(i);
        viewHolder.ageView.setText(w.getAge());
        viewHolder.nameView.setText(w.getName());
        viewHolder.idView.setText("#" + w.getId());
        viewHolder.positionView.setText(w.getPosition());
        viewHolder.photoView.setImageResource(w.getPhoto());
    }

    @Override
    public int getItemCount() {
        return workers.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(workers, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(workers, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        notifyItemChanged(fromPosition);
        notifyItemChanged(toPosition);
    }



    @Override
    public void onItemDismiss(int position) {
        workers.remove(position);
        notifyItemRemoved(position);
    }

    void dispatch(final List<Worker> newList) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MyDiffUtilCallback(getData(), newList));
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        diffResult.dispatchUpdatesTo(MyRecyclerAdapter.this);
                        workers.clear();
                        workers.addAll(newList);
                    }
                });
            }
        }).start();
    }

    public List<Worker> getData() {
        return workers;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView ageView, nameView, idView, positionView;
        private ImageView photoView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ageView = itemView.findViewById(R.id.ageView);
            nameView = itemView.findViewById(R.id.nameView);
            idView = itemView.findViewById(R.id.idView);
            photoView = itemView.findViewById(R.id.photoView);
            positionView = itemView.findViewById(R.id.positionView);
        }
    }
}